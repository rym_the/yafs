#!/usr/bin/env sh
export LC_ALL="ru_RU.UTF-8"

apk update && \
 apk add rsync curl make git musl-dev g++ patch openssl-dev file dos2unix icu icu-dev curl-dev

mkdir /tmp/1 && mkdir /tmp/2
cp patch.alpine.diff "/tmp/${YAFS_VERSION}.diff"
cd /tmp || exit

curl "https://archive.apache.org/dist/xerces/c/3/sources/${XEREC_VERSION}.tar.gz" | tar xz -C /tmp/1 && cd "/tmp/1/${XEREC_VERSION}" && ./configure && make && make install
curl -L "https://github.com/lhrios/yafs/archive/${YAFS_VERSION}.tar.gz" | tar xz -C /tmp/2 && cd "/tmp/2/yafs-${YAFS_VERSION}" && \
dos2unix ./* && patch < "/tmp/${YAFS_VERSION}.diff" && mkdir dep && mkdir bin && make -f Makefile_unix && \
cp "/tmp/2/yafs-${YAFS_VERSION}/fat_file_system_tree.xsd" /build && cp bin/yafs /build
cd /build && tar cvfz yafs.alpine.tar.gz fat_file_system_tree.xsd yafs && rm -f -- yafs fat_file_system_tree.xsd
rm -rf -- /tmp/1 /tmp/2